package org.minhha.ECDB;

import java.util.List;

import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.minhha.ECDB.mongodb.PartDBObject;
import org.minhha.ECDB.mongodb.PartDocument;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;

public class Example1 {
	private void partPrint(String name, PartDocument dbo) {
		System.out.println(name + dbo.getDouble(name));
	}
	public static void main( String[] args )
    {
		PartDBObject partDBObject = new PartDBObject();
		partDBObject.setName("STM32f303VCT6");
		
		PartDocument partDocument = new PartDocument();
		partDocument.setName("STM32f303VCT6");
		partDocument.setFootprint(8);
		partDocument.setQuantity(10000);
		
    	try{
//    		Tracking tk=new Tracking();
//    		tk.toDocument(new Date( ));
//    		System.out.println(Integer.MAX_VALUE);
            // To connect to mongodb server
    		MongoClientURI uri = new MongoClientURI("mongodb://vinh:vinh@localhost/?authSource=tracking");
    		MongoClient mongoClient = new MongoClient(uri);
   			
            // Now connect to your databases
            MongoDatabase db = mongoClient.getDatabase( "tracking" );
            DB db1 = mongoClient.getDB( "mydb" );
            for (String name : db.listCollectionNames()) {
                System.out.println("[COLLECTION]" + name);
            }
            MongoCollection<PartDocument> collection = db.getCollection("tracking", PartDocument.class);
//            DBCollection collection = db1.getCollection("tracking");
            System.out.println("Connect to database successfully");
            collection.insertOne(partDocument);
            System.out.println("Insert to to database successfully");

            FindIterable<PartDocument> find = collection.find(new BsonDocument());
            MongoCursor<PartDocument> cursor = find.iterator();
			try {
				for (int i=0; cursor.hasNext();i++) {
					BasicDBObject p = cursor.next();
					System.out.printf("[%d] ", i);
					System.out.print((((ObjectId)p.get("_id")).getDate()));
					System.out.print(" ");
					System.out.println(p);
				}
			} finally {
				cursor.close();
			}
   			
         }catch(Exception e){
            System.err.println( e.getClass().getName() + ": " + e.getMessage() );
         }
    }
}
