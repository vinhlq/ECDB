package org.minhha.ECDB.mongodb;
import java.util.Date;
import java.util.Map;

import org.bson.BsonInt32;
import org.bson.BsonInt64;
import org.bson.BsonString;
import org.bson.types.ObjectId;
import org.bson.BsonObjectId;
import com.mongodb.BasicDBObject;

import org.bson.BsonDateTime;


public class PartDBObject{
	private BasicDBObject document;

	public PartDBObject() {
		this.document = new BasicDBObject();
		this.document.put("_id", new ObjectId());
	}
	
	public PartDBObject(BasicDBObject document) {
		this.document = document;
	}
	
	public BasicDBObject getDocument() {
		return document;
	}
	
	public Date getDate() {
		return ((BsonObjectId)this.document.get("_id")).getValue().getDate();
	}
	public ObjectId getId() {
		return ((ObjectId)this.document.get("_id"));
	}
	public long getQuantity() {
		return ((BsonInt32)this.document.get("quantity")).getValue();
	}
	public void setQuantity(long quantity) {
		this.document.put("quantity", new BsonInt64(quantity));
	}
	public String getName() {
		return ((BsonString)this.document.get("name")).getValue();
	}
	public void setName(String name) {
		this.document.put("name", new BsonString(name));
	}
//	public String getMa_hop_dong() {
//		return ((BsonString)this.document.get("ma_hop_dong")).getValue();
//	}
//	public void setMa_hop_dong(String ma_hop_dong) {
//		this.document.put("name", new BsonString(ma_hop_dong));
//	}
	public int getType() {
		return ((BsonInt32)this.document.get("type")).getValue();
	}
	public void setType(int type) {
		this.document.put("type", new BsonInt32(type));
	}
	public String getIQC() {
		return ((BsonString)this.document.get("IQC")).getValue();
	}
	public void setIQC(String iQC) {
		this.document.put("IQC", new BsonString(iQC));
	}
	public int getPackageName() {
		return ((BsonInt32)this.document.get("packageName")).getValue();
	}
	public void setPackageName(int packageName) {
		this.document.put("packageName", new BsonInt32(packageName));
	}
	public int getFootprint() {
		return ((BsonInt32)this.document.get("footprint")).getValue();
	}
	public void setFootprint(int footprint) {
		this.document.put("footprint", new BsonInt32(footprint));
	}
	public int getSolderTemperature() {
		return ((BsonInt32)this.document.get("solder temperature")).getValue();
	}
	public void setSolderTemperature(int solderTemperature) {
		this.document.put("solder temperature", new BsonInt32(solderTemperature));
	}
	public String getFid() {
		return ((BsonString)this.document.get("fid")).getValue();
	}
	public void setFid(String fid) {
		this.document.put("fid", new BsonString(fid));
	} 
	
	
}