package org.minhha.ECDB.mongodb;
import java.util.Date;
import java.util.Map;

import org.bson.BsonInt32;
import org.bson.BsonInt64;
import org.bson.BsonString;
import org.bson.types.ObjectId;
import org.bson.BsonObjectId;
import com.mongodb.BasicDBObject;

import org.bson.BsonDateTime;


public class PartDocument extends BasicDBObject{
	/**
	 * 
	 */
	private static final long serialVersionUID = 9209756769835720516L;
	
	public PartDocument() {
        super();
        this.put("_id", new ObjectId());
    }
	public PartDocument(ObjectId objectId) {
        super();
        this.put("_id", objectId);
    }
	public Date getDate() {
		return ((ObjectId)this.get("_id")).getDate();
	}
	public ObjectId getId() {
		return (ObjectId)this.get("_id");
	}
	public long getQuantity() {
		return (long)this.getLong("quantity");
	}
	public void setQuantity(long quantity) {
		this.put("quantity", new BsonInt64(quantity));
	}
	public String getName() {
		return this.getString("name");
	}
	public void setName(String name) {
		this.put("name", new BsonString(name));
	}
	public String getContractId() {
		return ((BsonString)this.get("contract id")).getValue();
	}
	public void setContractId(String contractId) {
		this.put("contract id", new BsonString(contractId));
	}
	public int getType() {
		return ((BsonInt32)this.get("type")).getValue();
	}
	public void setType(int type) {
		this.put("type", new BsonInt32(type));
	}
	public int getValue() {
		return ((BsonInt32)this.get("value")).getValue();
	}
	public void setValue(int type) {
		this.put("value", new BsonInt32(type));
	}
	public String getIQC() {
		return ((BsonString)this.get("IQC")).getValue();
	}
	public void setIQC(String iQC) {
		this.put("IQC", new BsonString(iQC));
	}
	public int getPackageName() {
		return ((BsonInt32)this.get("packageName")).getValue();
	}
	public void setPackageName(int packageName) {
		this.put("packageName", new BsonInt32(packageName));
	}
	public int getFootprint() {
		return ((BsonInt32)this.get("footprint")).getValue();
	}
	public void setFootprint(int footprint) {
		this.put("footprint", new BsonInt32(footprint));
	}
	public int getSolderTemperature() {
		return ((BsonInt32)this.get("solder temperature")).getValue();
	}
	public void setSolderTemperature(int solderTemperature) {
		this.put("solder temperature", new BsonInt32(solderTemperature));
	}
	public String getFid() {
		return ((BsonString)this.get("fid")).getValue();
	}
	public void setFid(String fid) {
		this.put("fid", new BsonString(fid));
	}
}